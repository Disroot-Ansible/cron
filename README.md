# Cron - Ansible Role

This role covers deployment of crontab. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

## Use
You can deploy crontab by simply editing the `defaults/main.yml` file. For example `- '0 3 * * * username bash /home/username/your_script.sh'` to run a script at 3am everyday.